import React from "react"
import Layout from "./../components/layout"
import SEO from "../components/seo"
import Sidebar from "./../components/sidebar"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <div class="container">
      <div className="row">
        <div className="col-sm-8">Some Content goes here</div>
        <div className="col-sm-4">
          <Sidebar />
        </div>
      </div>
    </div>
  </Layout>
)

export default IndexPage
