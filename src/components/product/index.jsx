import React from "react"
import "./product.css"

export default () => (
  <div class="container-fluid pb-prod-cont">
    <div className="row">
      <div className="col">
        <h4 className="pb-prod-title">Bilancia singola</h4>
      </div>
    </div>
    <div className="row">
      <div className="col-6">
        <h6>
          <strike>€ 239.99</strike>
        </h6>
        <h6>
          € 220 <span>%</span>
        </h6>
        <h6>Prezzo pre-lancio</h6>
      </div>
      <div className="col-6">
        <h4 className="pb-prod-img">Image </h4>
      </div>
    </div>

    <div className="row pb-prod-desc">
      <div className="col">
        <h6>CARATTERISTICHE E SERVIZI</h6>
        <h5>
          <span>€220</span> - <span>239.99</span>
          <span>(28% sconto)</span>
        </h5>
        <ul>
          <li>267 acquistate / 500 disponibili</li>
          <li>Spedizione €10,000</li>
        </ul>
      </div>
    </div>
  </div>
)
