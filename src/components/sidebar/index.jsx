import React from "react"

import Product from "./../product"

import styles from "./sidebar.css"

export default () => (
  <div class="container-fluid">
    <Product />
    <hr />
    <Product />
  </div>
)
